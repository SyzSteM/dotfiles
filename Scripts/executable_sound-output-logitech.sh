#!/bin/bash

# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

# deactivate other cards
bash ~/Scripts/sound-mute-all.sh

# activate logitech headset
echo -e "$BOLD$BLUE::$WHITE Activating logitech headset...$RESET"
pactl set-card-profile alsa_card.usb-Logitech_PRO_X_000000000000-00 output:analog-stereo+input:mono-fallback
