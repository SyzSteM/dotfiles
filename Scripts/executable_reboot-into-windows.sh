#!/usr/bin/env bash

WINDOWS_TITLE=$(sudo rg -i "^menuentry 'Windows" /boot/grub2/grub.cfg | head -n 1 | cut -d "'" -f2)

echo "Set reboot target to: ${WINDOWS_TITLE}..."
sudo grub2-reboot "${WINDOWS_TITLE}"

echo "Rebooting system in 3 seconds..."
sleep 3
sudo reboot
