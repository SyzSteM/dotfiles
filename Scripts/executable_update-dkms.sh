#!/bin/bash

# Function to reinstall Tuxedo drivers
reinstall_tuxedo_drivers() {
    echo "Reinstalling tuxedo-drivers for kernel $1 with driver version $2..."

    # Remove the old version
    sudo dkms remove tuxedo-drivers/$2 --all

    # Add, build and install the new version
    sudo dkms add -m tuxedo-drivers -v $2
    sudo dkms build -m tuxedo-drivers -v $2 -k $1
    sudo dkms install -m tuxedo-drivers -v $2 -k $1
}

# Get the current kernel version
current_kernel_version=$(uname -r)

# Get the currently installed tuxedo-driver package version using rpm
current_tuxedo_driver_version=$(rpm -q tuxedo-drivers --qf "%{VERSION}")

# Get the current tuxedo-driver version from dkms status
dkms_tuxedo_driver_version=$(sudo dkms status | grep tuxedo-drivers | awk -F'/' '{print $2}' | awk -F',' '{print $1}')

# Get the installed kernel version for tuxedo-drivers from dkms status
dkms_tuxedo_driver_kernel_version=$(sudo dkms status | grep tuxedo-drivers | awk '{print $2}' | sed 's/,//' | awk -F', ' '{print $1}')

# Check for the '-f' option
while getopts "f" opt; do
    case $opt in
        f)
            reinstall_tuxedo_drivers $current_kernel_version $dkms_tuxedo_driver_version
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done

# Check if the kernel versions or tuxedo-driver versions do not match
if [[ "$current_kernel_version" != "$dkms_tuxedo_driver_kernel_version" || "$current_tuxedo_driver_version" != "$dkms_tuxedo_driver_version" ]]; then
    reinstall_tuxedo_drivers $current_kernel_version $current_tuxedo_driver_version
else
    echo "DKMS module 'tuxedo-drivers' is up to date with version $current_tuxedo_driver_version for kernel $current_kernel_version." 
fi

