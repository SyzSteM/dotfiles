#!/bin/bash

# variables
BOLD=$(tput bold)
RED=$(tput setaf 1)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

echo -e $BOLD$BLUE"::$WHITE Checking for Arch Linux news..."$RESET
yay -Pw

echo -e $BOLD$BLUE"::$WHITE Starting system update..."$RESET
yay -Syu

while true; do
    read -p "$BOLD$BLUE::$WHITE Do you want to clean the cache and remove unneeded packages? (y, N): $RESET" CLEAN_PACKAGES
    if [ -z $CLEAN_PACKAGES ]; then
        break
    fi
    case $CLEAN_PACKAGES in
    [yY]*)
        echo -e $BOLD$BLUE"::$WHITE Cleaning cache and removing untracked files..."$RESET
        yay -Sc --noconfirm

        echo -e $BOLD$BLUE"::$WHITE Removing unneeded dependencies..."$RESET
        yay -Yc --noconfirm

        echo -e $BOLD$BLUE"::$WHITE Displaying information about installed packages and system health..."$RESET
        yay -Ps

        break
        ;;
    [nN]*)
        break
        ;;
    *)
        echo $BOLD$RED"Please input 'y' or 'n'!"$RESET
        ;;
    esac
done
