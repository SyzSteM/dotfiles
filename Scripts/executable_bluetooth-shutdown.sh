#!/bin/bash

##############################
# Small script to shutdown   #
# everything bluetooth       #
##############################

# deactivate bluetooth
if [ $(systemctl is-active bluetooth) = "active" ]; then
	echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Deactivating agent...$(tput sgr0)"
	bluetoothctl agent off

	echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Turning off power...$(tput sgr0)"
	bluetoothctl power off

	echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Deactivating bluetooth...$(tput sgr0)"
	sudo systemctl stop bluetooth.service
fi
