#!/bin/bash

#--- BASIC SETUP
# shellcheck disable=1090
source "$HOME/Scripts/variables/basic.sh"

#--- PATHS
dotfiles_git_folder="$HOME/Git/dotfiles"

writeMessage "${BLUE}Restoring fstab file to /etc/...${RESET}"
sudo cp -ubiv --suffix .back "$dotfiles_git_folder/etc/fstab" /etc/

writeMessage "${BLUE}Restoring lightdm configs to /etc/lightdm/...${RESET}"
sudo cp -ubiv --suffix .back "$dotfiles_git_folder/etc/lightdm/lightdm.conf" /etc/lightdm/ 
sudo cp -ubiv --suffix .back "$dotfiles_git_folder/etc/lightdm/lightdm-webkit2-greeter.conf" /etc/lightdm/

writeMessage "${BLUE}Restoring snap-sync service and timer to /etc/systemd/system/...${RESET}"
sudo cp -ubiv --suffix .back "$dotfiles_git_folder/etc/systemd/system/snap-sync.timer" /etc/systemd/system/
sudo cp -ubiv --suffix .back "$dotfiles_git_folder/etc/systemd/system/snap-sync.service" /etc/systemd/system/
