#!/bin/bash

# variables
BOLD=$(tput bold)
RED=$(tput setaf 1)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

echo -e $BOLD$BLUE"::$WHITE Compiling server jar..."$RESET
cd "/home/timo/Development/university/semester_2/software-engineering-2/Tick-Tack-Bumm"
./gradlew jar

echo -e $BOLD$BLUE"::$WHITE Starting java server..."$RESET
cd "/home/timo/Development/university/semester_2/software-engineering-2/Tick-Tack-Bumm/server/build/libs"
java -jar server-1.0.jar

