#!/bin/bash

# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

echo -e "$BOLD$BLUE::$WHITE Copying template files...$RESET"
cp -ivr /home/timo/Templates/c_programm/. .
