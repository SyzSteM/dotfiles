#!/usr/bin/env bash

# Check if the input file is provided
if [ $# -eq 0 ]; then
    echo "Please provide a JSON file as input."
    exit 1
fi

# Get the input file name
input_file="$1"

# Extract the base name of the input file (without the extension)
base_name=$(basename "$input_file" .json)

# Create the output file name
output_file="${base_name}_formatted.json"

# Format the JSON using jq and save it to the output file
jq . "$input_file" > "$output_file"

echo "Formatted JSON saved to $output_file"
