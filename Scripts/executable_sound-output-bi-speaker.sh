#!/bin/bash

# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

# deactivate other cards
bash ~/Scripts/sound-mute-all.sh

# deactivate auto mute mode
if [ $(amixer -c 0 get 'Auto-Mute Mode' | grep Enabled | wc -l) -eq 2 ]; then
    echo -e "$BOLD$BLUE::$WHITE Deactivating Auto-Mute Mode...$RESET"
    amixer -c 0 sset -q 'Auto-Mute Mode' Disabled
fi

# activate built-in audio
echo -e "$BOLD$BLUE::$WHITE Activating built-in audio...$RESET"
pactl set-card-profile alsa_card.pci-0000_00_1f.3 output:analog-stereo+input:analog-stereo

# switch to built-in speaker
echo -e "$BOLD$BLUE::$WHITE Switching to speaker port...$RESET"
pactl set-sink-port alsa_output.pci-0000_00_1f.3.analog-stereo analog-output-speaker
