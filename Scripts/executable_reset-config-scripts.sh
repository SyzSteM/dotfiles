#!/bin/bash

#--- BASIC SETUP
# shellcheck disable=1090
source "$HOME/Scripts/variables/basic.sh"

she_bang="#!/bin/bash\n"

echo -e "$she_bang" >"$HOME"/Desktop/setup-configs.sh

echo -e "$she_bang" >"$HOME"/Desktop/restore-configs.sh

echo -e "$she_bang" >"$HOME"/Desktop/delete-configs.sh

echo -e "$she_bang" >"$HOME"/Desktop/replace-configs.sh

echo -e "$she_bang" >"$HOME"/Desktop/cleanup-configs.sh