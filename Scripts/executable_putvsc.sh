#!/bin/bash

#--- BASIC SETUP
# shellcheck disable=1090
source "$HOME/Scripts/variables/basic.sh"

setup_script_path="$HOME/Desktop/setup-configs.sh"
restore_script_path="$HOME/Desktop/restore-configs.sh"
delete_script_path="$HOME/Desktop/delete-configs.sh"
replace_script_path="$HOME/Desktop/replace-configs.sh"
cleanup_script_path="$HOME/Desktop/cleanup-configs.sh"

touch "$setup_script_path" "$restore_script_path" "$delete_script_path" "$replace_script_path" "$cleanup_script_path"
chmod +x "$setup_script_path" "$restore_script_path" "$delete_script_path" "$replace_script_path" "$cleanup_script_path"

#--- FUNCTIONS
put_file_under_vsc() {

	filename=$(basename "$1")
	org_file_path=$(readlink -f "$1")
	org_parent_folder=$(dirname "$org_file_path")
	git_repo_path="$HOME/Git/dotfiles"
	git_file_path="$git_repo_path$org_file_path"
	git_folder_structure=$(dirname "$git_file_path")

	writeMessage "${BLUE}$filename: Creating folder structure $git_folder_structure for VSC...${RESET}"
  sudo mkdir -p "$git_folder_structure"

	writeMessage "${GREEN}$filename: Copying $org_file_path to $git_file_path${RESET}..."
	sudo cp -f "$org_file_path" "$git_file_path"

	writeMessage "${PURPLE}$filename: Creating symbolic link from $git_file_path to $org_file_path...${RESET}"
	sudo ln -sb --suffix .back "$git_file_path" "$org_file_path"

	# setup script
	if [ "$(rg -uu "$org_file_path" "$setup_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$filename: Adding $filename with path $org_file_path to 'setup-configs.sh'...${RESET}"

		{
			echo "#--- $filename"
			echo -e "if [ ! -L $org_file_path ]; then\n	sudo mkdir -p $org_parent_folder\n	sudo ln -sbv --suffix .back $git_file_path $org_file_path\nfi\n"
		} >>"$setup_script_path"

	else
		writeMessage "${RED}$filename: File $filename with path $org_file_path is already included in 'setup-configs.sh'...${RESET}"

	fi

	# restore script
	if [ "$(rg -uu "$org_file_path" "$restore_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$filename: Adding $filename with path $org_file_path to 'restore-configs.sh'...${RESET}"

		{
			echo "#--- $filename"
			echo "sudo mv -fv ${org_file_path}.back $org_file_path"
			echo
		} >>"$restore_script_path"

	else
		writeMessage "${RED}$filename: File $filename with path $org_file_path is already included in 'restore-configs.sh'...${RESET}"

	fi

	# delete script
	if [ "$(rg -uu "$org_file_path" "$delete_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$filename: Adding $filename with path $org_file_path to 'delete-configs.sh'...${RESET}"

		{
			echo "#--- $filename"
			echo "sudo rm -fv $git_file_path"
			echo
		} >>"$delete_script_path"

	else
		writeMessage "${RED}$filename: File $filename with path $org_file_path is already included in 'delete-configs.sh'...${RESET}"

	fi

	# replace script
	if [ "$(rg -uu "$org_file_path" "$replace_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$filename: Adding $filename with path $org_file_path to 'replace-configs.sh'...${RESET}"

		{
			echo "#--- $filename"
			echo -e "if [ -L $org_file_path ]; then\n	sudo rm -fv $org_file_path\n	sudo cp -fv $git_file_path $org_file_path\nfi"
			echo
		} >>"$replace_script_path"

	else

		writeMessage "${RED}$filename: File $filename with path $org_file_path is already included in 'replace-configs.sh'...${RESET}"

	fi

	# cleanup script
	if [ "$(rg -uu "$org_file_path" "$cleanup_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$filename: Adding $filename with path $org_file_path to 'cleanup-configs.sh'...${RESET}"

		{
			echo "#--- $filename"
			echo "sudo rm -fv ${org_file_path}.back"
			echo
		} >>"$cleanup_script_path"

	else
		writeMessage "${RED}$filename: File $filename with path $org_file_path is already included in 'cleanup-configs.sh'...${RESET}"

	fi

	echo

}

put_folder_under_vsc() {

	foldername=$(basename "$1")
	org_folder_path=$(readlink -f "$1")
	org_parent_folder=$(dirname "$org_folder_path")
	git_repo_path="$HOME/Git/dotfiles"
	git_folder_path="$git_repo_path$org_folder_path"
	git_folder_structure=$(dirname "$git_folder_path")

	writeMessage "${BLUE}$foldername: Creating folder structure $git_folder_path for VSC...${RESET}"
	sudo mkdir -p "$git_folder_structure"

	writeMessage "${GREEN}$foldername: Copying contents of $org_folder_path to $git_folder_path${RESET}..."
	sudo cp -rfv "$org_folder_path" "$git_folder_structure"

	writeMessage "${PURPLE}$foldername: Backing up folder $org_folder_path to ${org_folder_path}.back...${RESET}"
	sudo mv -f "$org_folder_path" "${org_folder_path}.back"

	writeMessage "${PURPLE}$foldername: Creating symbolic link from $git_folder_path to $org_folder_path...${RESET}"
	sudo ln -sb --suffix .back "$git_folder_path" "$org_folder_path"

	# setup script
	if [ "$(rg -uu "$org_folder_path" "$setup_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$foldername: Adding $foldername with path $org_folder_path to 'setup-configs.sh'...${RESET}"

		{
			echo "#--- $foldername"
			echo -e "if [ ! -L $org_folder_path ]; then\n	sudo mkdir -p $org_parent_folder\n	sudo mv -f $org_folder_path ${org_folder_path}.back\n	sudo ln -sv $git_folder_path $org_folder_path\nfi\n"
		} >>"$setup_script_path"

	else
		writeMessage "${RED}$foldername: Folder $foldername with path $org_folder_path is already included in 'setup-configs.sh'...${RESET}"

	fi

	# restore script
	if [ "$(rg -uu "$org_folder_path" "$restore_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$foldername: Adding $foldername with path $org_folder_path to 'restore-configs.sh'...${RESET}"

		{
			echo "#--- $foldername"
			echo "sudo rm -fv $org_folder_path"
			echo "sudo mv -fv ${org_folder_path}.back $org_folder_path"
			echo
		} >>"$restore_script_path"

	else
		writeMessage "${RED}$foldername: Folder $foldername with path $org_folder_path is already included in 'restore-configs.sh'...${RESET}"

	fi

	# delete script
	if [ "$(rg -uu "$org_folder_path" "$delete_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$foldername: Adding $foldername with path $org_folder_path to 'delete-configs.sh'...${RESET}"

		{
			echo "#--- $foldername"
			echo "sudo rm -rfv $git_folder_path"
			echo
		} >>"$delete_script_path"

	else
		writeMessage "${RED}$foldername: Folder $foldername with path $org_folder_path is already included in 'delete-configs.sh'...${RESET}"

	fi

	# replace script
	if [ "$(rg -uu "$org_folder_path" "$replace_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$foldername: Adding $foldername with path $org_folder_path to 'replace-configs.sh'...${RESET}"

		{
			echo "#--- $foldername"
			echo -e "if [ -L $org_folder_path ]; then\n	sudo rm -fv $org_folder_path\n	sudo cp -rfv $git_folder_path $org_folder_path\nfi"
			echo
		} >>"$replace_script_path"

	else

		writeMessage "${RED}$foldername: Folder $foldername with path $org_folder_path is already included in 'replace-configs.sh'...${RESET}"

	fi

	# cleanup script
	if [ "$(rg -uu "$org_folder_path" "$cleanup_script_path" | wc -l)" -eq 0 ]; then
		writeMessage "${YELLOW}$foldername: Adding $foldername with path $org_folder_path to 'cleanup-configs.sh'...${RESET}"

		{
			echo "#--- $foldername"
			echo "sudo rm -rfv ${org_folder_path}.back"
			echo
		} >>"$cleanup_script_path"

	else
		writeMessage "${RED}$foldername: Folder $foldername with path $org_folder_path is already included in 'cleanup-configs.sh'...${RESET}"

	fi

	echo

}

#--- SCRIPT
for path in "${@%/}"; do
	if [ -L "$path" ]; then
		errorMessage "This tool only works for files and folders. Symlinks are excluded for safety purposes!"

	elif [ -d "$path" ]; then
		put_folder_under_vsc "$path"

	elif [ -f "$path" ]; then
		put_file_under_vsc "$path"

	else
		errorMessage "The path $path can not be handled! The path does not exists or the type is not supported."

	fi
done
