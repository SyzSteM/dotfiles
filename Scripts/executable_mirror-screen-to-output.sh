#!/bin/bash

################################
# Small script to duplicate    #
# internal screen on a         #
# projector using xrandr.      #
################################

# list all outputs
xrandr

# get output
echo -e "\n$(tput bold)Specify output:$(tput sgr0)"
read i_output

# get mode
echo -e "\n$(tput bold)Specify mode:$(tput sgr0)"
read i_mode

# duplicate internal screen to selected output
xrandr --output $i_output --same-as DP-0 --mode $i_mode
