#! /usr/bin/env bash

cd /home/timo/Development/plincs/basf
scp COBA/target/COBA-*.war root@xport-basf.plincs.com:/root/bemi.war
scp GUI/target/GUI-*.war root@xport-basf.plincs.com:/root/xport.war
