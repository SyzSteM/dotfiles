#!/bin/bash

#--- BASIC SETUP
# shellcheck disable=1090
source "$HOME/Scripts/variables/basic.sh"

#--- PATHS
dotfiles_git_folder="$HOME/Git/dotfiles"

writeMessage "${BLUE}Backing up fstab file to $dotfiles_git_folder/etc/...${RESET}"
sudo cp -uv /etc/fstab "$dotfiles_git_folder/etc/"

writeMessage "${BLUE}Backing up lightdm configs to $dotfiles_git_folder/etc/lightdm/...${RESET}"
sudo cp -uv /etc/lightdm/lightdm.conf "$dotfiles_git_folder/etc/lightdm/"
sudo cp -uv /etc/lightdm/lightdm-webkit2-greeter.conf "$dotfiles_git_folder/etc/lightdm/"

writeMessage "${BLUE}Backing up snap-sync service and timer to $dotfiles_git_folder/etc/systemd/system/...${RESET}"
sudo cp -uv /etc/systemd/system/snap-sync.timer "$dotfiles_git_folder/etc/systemd/system/"
sudo cp -uv /etc/systemd/system/snap-sync.service "$dotfiles_git_folder/etc/systemd/system/"
