#!/bin/bash

# Define the URL and the shortcut name
URL=$1
NAME=$2

# Define the directory where shortcuts will be created
SHORTCUT_DIR="$HOME/Desktop"

# Launch Chromium to create the shortcut
chromium --headless --disable-gpu --profile-directory="Default" --app="$URL" --create-shortcut="$SHORTCUT_DIR/$NAME"

echo "Shortcut created for $URL named $NAME"

