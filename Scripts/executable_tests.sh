#!/bin/bash

currentDir="$( dirname "$( command -v "$0" )" )"
scriptsCommonUtilities="$( dirname "$currentDir/Scripts" )/scripts-common/utilities.sh"
[ ! -f "$scriptsCommonUtilities" ] && echo -e "ERROR: scripts-common utilities not found, you must install it before using this script (checked path: $scriptsCommonUtilities)" >&2 && exit 1
# shellcheck disable=1090
. "$scriptsCommonUtilities"


# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

#------------ VARIABLES
HOSTNAME=ricebowl
USERNAME=andrew
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TOUCHPAD_CONF=/etc/xorg.conf.d/X11/90-touchpad.conf

echo $SCRIPT_DIR

writeMessage "TEST MESSAGE..."

#------------ PERMISSION CHECK
# id -u, also gets the current user's id
if [ "$EUID" -ne 0 ]
	then echo "Please run this script with root permission."

fi

if ! isRootUser; then
	errorMessage "This script must be launched with root user."
fi

#------------ SETUP EXECUTION
read -p "Setup script ready to run, press ENTER to start."

# Check if program is installed
if [ ! -x /usr/bin/kekw ] ; then
    # fallback check using posix compliant `command` in case not installed in expected location
    command -v wget >/dev/null 2>&1 || { echo >&2 "Please install kekw. Aborting."; }
fi

if ! command -v KEKW &> /dev/null
then
    echo "KEKW could not be found"
fi

command -v foo >/dev/null 2>&1 || { echo >&2 "I require foo but it's not installed.  Aborting."; }
type foo >/dev/null 2>&1 || { echo >&2 "I require foo but it's not installed.  Aborting."; }
hash foo 2>/dev/null || { echo >&2 "I require foo but it's not installed.  Aborting.";  }

checkBin speedtest || errorMessage "This tool requires jq. Install it please, and then run this tool again."

checkDataFile "/usr/share/doc/myApp/myApp.rules" || errorMessage "Unable to find Data file (See documentation). Create it please, and then run this tool again."
