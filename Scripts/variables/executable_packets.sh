#!/bin/bash
# shellcheck disable=2034

DisplayManager=(
    lightdm
    lightdm-webkit2-greeter
    lightdm-webkit-theme-litarvan
)

Rofi=(
    rofi
    rofimoji
)

Multimedia=(
    evince
    feh
    spotify
    vlc
)

Compositor=(
    picom
)

Polybar=(
    polybar
)

Sound=(
    alsa-utils
    playerctl
    pulseaudio
    pulseaudio-alsa
    pulseaudio-bluetooth
    pulsemixer
)

Keyboard=(
    xbindkeys
)

Filemanager=(
    nemo
    ntfs-3g
)

PacmanUtils=(
    aur-auto-vote
    expac
    paccache-hook
)

Themes=(
    arc-gtk-theme
    arc-icon-theme
    capitaine-cursors
)

SystemUtils=(
    aria2
    git
    flameshot
    libgnome-keyring
    light-locker
    man-pages
    network-manager-applet
    nvidia-settings
    openssh
    pwgen
    redshift
    unzip
    wget
)

Rust=(
    rustup
)

RustTools=(
    alacritty
    bat
    bottom
    bingrep
    dust
    eva
    exa
    fd
    hex
    kmon
    procs
    ripgrep
    skim
    starship
    tokei
    zoxide
)

declare -A FontDict
FontDict["nerd-fonts-complete"]="/usr/share/fonts/nerd-fonts-complete"
FontDict["siji-git"]="/usr/share/fonts/misc/siji.bdf"
FontDict["ttf-jetbrains-mono"]="/usr/share/fonts/TTF/JetBrainsMono-Regular.ttf"

UserSoftware=(
    brave-bin
    discord
    obs-studio
    onedrive-abraunegg
    rpiplay-git
    telegram-desktop
    translate-shell
)

DevSoftware=(
    code
    deno
    jdk-openjdk
    intellij-idea-ultimate-edition
    intellij-idea-ultimate-edition-jre
)

Neovim=(
    neovim
    neovim-plug
    neovim-symlinks
)
