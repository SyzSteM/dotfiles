#!/bin/bash

#--- ENABLE BASH STRICT MODE
set -euo pipefail
IFS=$'\n\t'

#--- INCLUDE SCRIPTS-COMMON
scriptsCommonUtilities="$HOME/Git/software/scripts-common/utilities.sh"
[ ! -f "$scriptsCommonUtilities" ] && echo -e "ERROR: scripts-common utilities not found, you must install it before using this script (checked path: $scriptsCommonUtilities)" >&2 && exit 1
# shellcheck disable=1090
. "$scriptsCommonUtilities"

export BSC_MODE_CHECK_CONFIG=1 # check does NOT exit the script

#--- STYLING VARIABLES
# shellcheck disable=1090
source "$HOME/Scripts/variables/styling.sh"