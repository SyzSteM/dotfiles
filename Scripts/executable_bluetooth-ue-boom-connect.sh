#!/bin/bash

##############################
# Small script to connect to #
# the UE Boom box            #
##############################

# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

# activate bluetooth and disconnect other device
if [ $(systemctl is-active bluetooth) = "inactive" ]; then
    echo -e "$BOLD$BLUE::$WHITE Activating bluetooth...$RESET"
    sudo systemctl start bluetooth.service
    sleep 1

elif [ $(bluetoothctl info | grep "Device 20:19:05:51:57:01" | wc -l) -eq 1 ]; then
    echo -e "$BOLD$BLUE::$WHITE Disconnecting Mpow D9...$RESET"
    bluetoothctl disconnect 20:19:05:51:57:01

fi

# check if device is not already connected
if [ $(bluetoothctl info | grep "Device 88:C6:26:03:6E:E6" | wc -l) -eq 0 ]; then
    # use bluetoothctl to connect to device
    echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Turning on power...$(tput sgr0)"
    bluetoothctl power on

    echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Activating agent...$(tput sgr0)"
    bluetoothctl agent on
    bluetoothctl list

    echo -e "$(tput bold)$(tput setaf 6)::$(tput setaf 7) Connecting to UE Boom...$(tput sgr0)"
    bluetoothctl connect 88:C6:26:03:6E:E6
    sleep 1

fi

# deactive other cards
bash ~/Scripts/sound-mute-all.sh

# activate A2DP profile
echo -e "$BOLD$BLUE::$WHITE Turning on A2DP profile...$RESET"
pactl set-card-profile bluez_card.88_C6_26_03_6E_E6 a2dp_sink
