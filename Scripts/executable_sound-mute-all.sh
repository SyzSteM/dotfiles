#!/bin/bash

# variables
BOLD="$(tput bold)"
BLUE="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
RESET="$(tput sgr0)"

echo "$BOLD$BLUE::$WHITE Muting all cards...$RESET"

for CARD in $(pactl list short cards | awk '{print $2}'); do
    pactl set-card-profile $CARD off
done
