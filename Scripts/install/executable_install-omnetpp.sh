#!/bin/bash

##############################
# Install omnetpp tool       #
##############################

# styling variables
BOLD=$(tput bold)
RED=$(tput setaf 1)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

CLONE_FOLDER="/home/$USER/Downloads/git_clones"
SOURCE_FOLDER="$CLONE_FOLDER/omnetpp"

echo -e $BOLD$BLUE"::$WHITE Installing dependencies..."$RESET
yay -S --needed --noconfirm openscenegraph git

echo -e $BOLD$BLUE"::$WHITE Cleaning folder structure..."$RESET
rm -rf $SOURCE_FOLDER

echo -e $BOLD$BLUE"::$WHITE Creating folder structure $CLONE_FOLDER ..."$RESET
mkdir -p $CLONE_FOLDER
cd $CLONE_FOLDER

echo -e $BOLD$BLUE"::$WHITE Cloning repo to $SOURCE_FOLDER ..."$RESET
git clone https://github.com/omnetpp/omnetpp.git
cd $SOURCE_FOLDER

echo -e $BOLD$BLUE"::$WHITE Configuring build with osgearth..."$RESET
source setenv
cp configure.user.dist configure.user
./configure WITH_OSGEARTH=no

echo -e $BOLD$BLUE"::$WHITE Building omnetpp..."$RESET
make -j8

echo -e $BOLD$BLUE"::$WHITE Add $SOURCE_FOLDER/bin to your path!!!"$RESET