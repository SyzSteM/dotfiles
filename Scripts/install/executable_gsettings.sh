#!/bin/bash

# styling variables
BOLD=$(tput bold)
RED=$(tput setaf 1)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

echo -e $BOLD$BLUE"::$WHITE Setting custom GNOME keyboard settings..."$RESET
gsettings set org.gnome.desktop.peripherals.keyboard delay 300
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 20

echo -e $BOLD$BLUE"::$WHITE Setting custom GNOME theme settings..."$RESET
gsettings set org.gnome.desktop.interface icon-theme 'Yaru-Blue'
gsettings set org.gnome.desktop.interface gtk-theme 'Yaru-Blue'
gsettings set org.gnome.desktop.interface cursor-theme 'breeze_cursors'

echo -e $BOLD$BLUE"::$WHITE Setting custom GNOME default applications..."$RESET
gsettings set org.gnome.desktop.default-applications.terminal exec 'alacritty'
gsettings set org.gnome.desktop.default-applications.terminal exec-arg ''
