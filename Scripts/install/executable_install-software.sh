#!/bin/bash

##############################
# Small system setup script  #
##############################

#--- BASIC SETUP
# shellcheck disable=1090
source "$HOME/Scripts/variables/basic.sh"

#--- PACKET LISTS
# shellcheck disable=1090
source "$HOME/Scripts/variables/packets.sh"
declare DisplayManager FontDict Rofi Multimedia Compositor Polybar Sound Keyboard Filemanager PacmanUtils Themes SystemUtils Rust RustTools UserSoftware DevSoftware

#--- FUNCTIONS
bin_checked_install() {
    for packet in "$@"; do
        writeMessage "${YELLOW}Checking $packet...${RESET}"
        if ! checkBin "$packet"; then
            install_with_yay "$packet"
        fi
    done
}

path_checked_install() {
    for font in $1; do
        writeMessage "${YELLOW}Checking font $font...${RESET}"
        if ! checkPath "${FontDict[$font]}"; then
            install_with_yay "$font"
        fi
    done
}

install_with_yay() {
    writeMessage "${GREEN}Installing $1...${RESET}"
    yay -S --needed --noconfirm "$1"
}

#--- SCRIPT
writeMessage "${BLUE}SYSTEM UPDATE${RESET}"
sudo pacman -Syyu

echo
writeMessage "${YELLOW}Checking yay...${RESET}"
if ! checkBin yay; then
    echo
    writeMessage "${GREEN}Installing yay...${RESET}"
    git clone https://aur.archlinux.org/yay-bin.git
    cd yay-bin
    makepkg -si
    cd ..
    rm -rf yay-bin
fi

echo
writeMessage "${BLUE}BASE-DEVEL${RESET}"
install_with_yay "base-devel"

echo
writeMessage "${BLUE}DISPLAY MANAGER${RESET}"
bin_checked_install "${DisplayManager[@]}"
writeMessage "${PURPLE}Enabling lightdm service...${RESET}"
sudo systemctl enable lightdm.service

echo
writeMessage "${BLUE}ROFI${RESET}"
bin_checked_install "${Rofi[@]}"

echo
writeMessage "${BLUE}MULTIMEDIA VIEWERS${RESET}"
bin_checked_install "${Multimedia[@]}"

echo
writeMessage "${BLUE}COMPOSITOR${RESET}"
bin_checked_install "${Compositor[@]}"

echo
writeMessage "${BLUE}BAR${RESET}"
bin_checked_install "${Polybar[@]}"

echo
writeMessage "${BLUE}SOUND${RESET}"
bin_checked_install "${Sound[@]}"

echo
writeMessage "${BLUE}KEYBOARD${RESET}"
bin_checked_install "${Keyboard[@]}"

echo
writeMessage "${BLUE}FILEMANAGER${RESET}"
bin_checked_install "${Filemanager[@]}"

echo
writeMessage "${BLUE}PACMAN UTILS${RESET}"
bin_checked_install "${PacmanUtils[@]}"

echo
writeMessage "${BLUE}THEMES${RESET}"
bin_checked_install "${Themes[@]}"

echo
writeMessage "${BLUE}SYSTEM UTILS${RESET}"
bin_checked_install "${SystemUtils[@]}"
writeMessage "${PURPLE}Enabling redshift service...${RESET}"
systemctl --user enable --now redshift

echo
writeMessage "${BLUE}RUST${RESET}"
bin_checked_install "${Rust[@]}"
writeMessage "${PURPLE}Updating rust...${RESET}"
rustup update stable
writeMessage "${PURPLE}Installing nightly toolchain...${RESET}"
rustup toolchain install nightly

echo
writeMessage "${BLUE}RUST SYSTEM TOOLS${RESET}"
bin_checked_install "${RustTools[@]}"

echo
writeMessage "${BLUE}FONTS${RESET}"
path_checked_install "${!FontDict[*]}"
writeMessage "${PURPLE}Rebuilding font cache...${RESET}"
fc-cache -fv

echo
writeMessage "${BLUE}USER SOFTWARE${RESET}"
bin_checked_install "${UserSoftware[@]}"

echo
writeMessage "${BLUE}DEVELOPMENT SOFTWARE${RESET}"
bin_checked_install "${DevSoftware[@]}"
