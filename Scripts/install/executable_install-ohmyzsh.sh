#!/bin/bash

# styling variables
BOLD=$(tput bold)
RED=$(tput setaf 1)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

echo -e $BOLD$BLUE"::$WHITE Installing oh-my-zsh..."$RESET
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended --keep-zshrc
