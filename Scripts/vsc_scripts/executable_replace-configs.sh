#!/bin/bash

#--- .aliases
if [ -L /home/timo/.aliases ]; then
	sudo rm -fv /home/timo/.aliases
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.aliases /home/timo/.aliases
fi

#--- .gitconfig
if [ -L /home/timo/.gitconfig ]; then
	sudo rm -fv /home/timo/.gitconfig
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.gitconfig /home/timo/.gitconfig
fi

#--- .xbindkeysrc
if [ -L /home/timo/.xbindkeysrc ]; then
	sudo rm -fv /home/timo/.xbindkeysrc
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.xbindkeysrc /home/timo/.xbindkeysrc
fi

#--- .zshenv
if [ -L /home/timo/.zshenv ]; then
	sudo rm -fv /home/timo/.zshenv
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.zshenv /home/timo/.zshenv
fi

#--- .zshrc
if [ -L /home/timo/.zshrc ]; then
	sudo rm -fv /home/timo/.zshrc
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.zshrc /home/timo/.zshrc
fi

#--- index.theme
if [ -L /home/timo/.icons/default/index.theme ]; then
	sudo rm -fv /home/timo/.icons/default/index.theme
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.icons/default/index.theme /home/timo/.icons/default/index.theme
fi

#--- Scripts
if [ -L /home/timo/Scripts ]; then
	sudo rm -fv /home/timo/Scripts
	sudo cp -rfv /home/timo/Git/dotfiles/home/timo/Scripts /home/timo/Scripts
fi

#--- alacritty.yml
if [ -L /home/timo/.config/alacritty/alacritty.yml ]; then
	sudo rm -fv /home/timo/.config/alacritty/alacritty.yml
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/alacritty/alacritty.yml /home/timo/.config/alacritty/alacritty.yml
fi

#--- aria2.conf
if [ -L /home/timo/.config/aria2/aria2.conf ]; then
	sudo rm -fv /home/timo/.config/aria2/aria2.conf
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/aria2/aria2.conf /home/timo/.config/aria2/aria2.conf
fi

#--- snippets
if [ -L /home/timo/.config/Code/User/snippets ]; then
	sudo rm -fv /home/timo/.config/Code/User/snippets
	sudo cp -rfv /home/timo/Git/dotfiles/home/timo/.config/Code/User/snippets /home/timo/.config/Code/User/snippets
fi

#--- settings.ini
if [ -L /home/timo/.config/gtk-3.0/settings.ini ]; then
	sudo rm -fv /home/timo/.config/gtk-3.0/settings.ini
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/gtk-3.0/settings.ini /home/timo/.config/gtk-3.0/settings.ini
fi

#--- config
if [ -L /home/timo/.config/i3/config ]; then
	sudo rm -fv /home/timo/.config/i3/config
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/i3/config /home/timo/.config/i3/config
fi

#--- config
if [ -L /home/timo/.config/onedrive/config ]; then
	sudo rm -fv /home/timo/.config/onedrive/config
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive/config /home/timo/.config/onedrive/config
fi

#--- business_shared_folders
if [ -L /home/timo/.config/onedrive-uni/business_shared_folders ]; then
	sudo rm -fv /home/timo/.config/onedrive-uni/business_shared_folders
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/business_shared_folders /home/timo/.config/onedrive-uni/business_shared_folders
fi

#--- config
if [ -L /home/timo/.config/onedrive-uni/config ]; then
	sudo rm -fv /home/timo/.config/onedrive-uni/config
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/config /home/timo/.config/onedrive-uni/config
fi

#--- picom.conf
if [ -L /home/timo/.config/picom/picom.conf ]; then
	sudo rm -fv /home/timo/.config/picom/picom.conf
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/picom/picom.conf /home/timo/.config/picom/picom.conf
fi

#--- polybar
if [ -L /home/timo/.config/polybar ]; then
	sudo rm -fv /home/timo/.config/polybar
	sudo cp -rfv /home/timo/Git/dotfiles/home/timo/.config/polybar /home/timo/.config/polybar
fi

#--- redshift.conf
if [ -L /home/timo/.config/redshift/redshift.conf ]; then
	sudo rm -fv /home/timo/.config/redshift/redshift.conf
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/redshift/redshift.conf /home/timo/.config/redshift/redshift.conf
fi

#--- config.rasi
if [ -L /home/timo/.config/rofi/config.rasi ]; then
	sudo rm -fv /home/timo/.config/rofi/config.rasi
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/rofi/config.rasi /home/timo/.config/rofi/config.rasi
fi

#--- starship.toml
if [ -L /home/timo/.config/starship.toml ]; then
	sudo rm -fv /home/timo/.config/starship.toml
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/starship.toml /home/timo/.config/starship.toml
fi

#--- mpris-proxy.service
if [ -L /home/timo/.config/systemd/user/mpris-proxy.service ]; then
	sudo rm -fv /home/timo/.config/systemd/user/mpris-proxy.service
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/systemd/user/mpris-proxy.service /home/timo/.config/systemd/user/mpris-proxy.service
fi

#--- config.json
if [ -L /home/timo/.config/yay/config.json ]; then
	sudo rm -fv /home/timo/.config/yay/config.json
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/yay/config.json /home/timo/.config/yay/config.json
fi

#--- youtube-dl
if [ -L /home/timo/.config/youtube-dl ]; then
	sudo rm -fv /home/timo/.config/youtube-dl
	sudo cp -rfv /home/timo/Git/dotfiles/home/timo/.config/youtube-dl /home/timo/.config/youtube-dl
fi

#--- lightdm.conf
if [ -L /etc/lightdm/lightdm.conf ]; then
	sudo rm -fv /etc/lightdm/lightdm.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf
fi

#--- lightdm-webkit2-greeter.conf
if [ -L /etc/lightdm/lightdm-webkit2-greeter.conf ]; then
	sudo rm -fv /etc/lightdm/lightdm-webkit2-greeter.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/lightdm/lightdm-webkit2-greeter.conf /etc/lightdm/lightdm-webkit2-greeter.conf
fi

#--- pacman.conf
if [ -L /etc/pacman.conf ]; then
	sudo rm -fv /etc/pacman.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/pacman.conf /etc/pacman.conf
fi

#--- mirrorupgrade.hook
if [ -L /etc/pacman.d/hooks/mirrorupgrade.hook ]; then
	sudo rm -fv /etc/pacman.d/hooks/mirrorupgrade.hook
	sudo cp -fv /home/timo/Git/dotfiles/etc/pacman.d/hooks/mirrorupgrade.hook /etc/pacman.d/hooks/mirrorupgrade.hook
fi

#--- logind.conf
if [ -L /etc/systemd/logind.conf ]; then
	sudo rm -fv /etc/systemd/logind.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/systemd/logind.conf /etc/systemd/logind.conf
fi

#--- 00-keyboard.conf
if [ -L /etc/X11/xorg.conf.d/00-keyboard.conf ]; then
	sudo rm -fv /etc/X11/xorg.conf.d/00-keyboard.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
fi

#--- 30-touchpad.conf
if [ -L /etc/X11/xorg.conf.d/30-touchpad.conf ]; then
	sudo rm -fv /etc/X11/xorg.conf.d/30-touchpad.conf
	sudo cp -fv /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf
fi

#--- onedrive.service
if [ -L /usr/lib/systemd/user/onedrive.service ]; then
	sudo rm -fv /usr/lib/systemd/user/onedrive.service
	sudo cp -fv /home/timo/Git/dotfiles/usr/lib/systemd/user/onedrive.service /usr/lib/systemd/user/onedrive.service
fi

#--- timo
if [ -L /var/spool/cron/timo ]; then
	sudo rm -fv /var/spool/cron/timo
	sudo cp -fv /home/timo/Git/dotfiles/var/spool/cron/timo /var/spool/cron/timo
fi

#--- init.vim
if [ -L /home/timo/.config/nvim/init.vim ]; then
	sudo rm -fv /home/timo/.config/nvim/init.vim
	sudo cp -fv /home/timo/Git/dotfiles/home/timo/.config/nvim/init.vim /home/timo/.config/nvim/init.vim
fi

#--- snap-sync.service
if [ -L /etc/systemd/system/snap-sync.service ]; then
	sudo rm -fv /etc/systemd/system/snap-sync.service
	sudo cp -fv /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.service /etc/systemd/system/snap-sync.service
fi

#--- snap-sync.timer
if [ -L /etc/systemd/system/snap-sync.timer ]; then
	sudo rm -fv /etc/systemd/system/snap-sync.timer
	sudo cp -fv /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.timer /etc/systemd/system/snap-sync.timer
fi

