#!/bin/bash

#--- .aliases
sudo mv -fv /home/timo/.aliases.back /home/timo/.aliases

#--- .gitconfig
sudo mv -fv /home/timo/.gitconfig.back /home/timo/.gitconfig

#--- .xbindkeysrc
sudo mv -fv /home/timo/.xbindkeysrc.back /home/timo/.xbindkeysrc

#--- .zshenv
sudo mv -fv /home/timo/.zshenv.back /home/timo/.zshenv

#--- .zshrc
sudo mv -fv /home/timo/.zshrc.back /home/timo/.zshrc

#--- index.theme
sudo mv -fv /home/timo/.icons/default/index.theme.back /home/timo/.icons/default/index.theme

#--- Scripts
sudo rm -fv /home/timo/Scripts
sudo mv -fv /home/timo/Scripts.back /home/timo/Scripts

#--- alacritty.yml
sudo mv -fv /home/timo/.config/alacritty/alacritty.yml.back /home/timo/.config/alacritty/alacritty.yml

#--- aria2.conf
sudo mv -fv /home/timo/.config/aria2/aria2.conf.back /home/timo/.config/aria2/aria2.conf

#--- snippets
sudo rm -fv /home/timo/.config/Code/User/snippets
sudo mv -fv /home/timo/.config/Code/User/snippets.back /home/timo/.config/Code/User/snippets

#--- settings.ini
sudo mv -fv /home/timo/.config/gtk-3.0/settings.ini.back /home/timo/.config/gtk-3.0/settings.ini

#--- config
sudo mv -fv /home/timo/.config/i3/config.back /home/timo/.config/i3/config

#--- config
sudo mv -fv /home/timo/.config/onedrive/config.back /home/timo/.config/onedrive/config

#--- business_shared_folders
sudo mv -fv /home/timo/.config/onedrive-uni/business_shared_folders.back /home/timo/.config/onedrive-uni/business_shared_folders

#--- config
sudo mv -fv /home/timo/.config/onedrive-uni/config.back /home/timo/.config/onedrive-uni/config

#--- picom.conf
sudo mv -fv /home/timo/.config/picom/picom.conf.back /home/timo/.config/picom/picom.conf

#--- polybar
sudo rm -fv /home/timo/.config/polybar
sudo mv -fv /home/timo/.config/polybar.back /home/timo/.config/polybar

#--- redshift.conf
sudo mv -fv /home/timo/.config/redshift/redshift.conf.back /home/timo/.config/redshift/redshift.conf

#--- config.rasi
sudo mv -fv /home/timo/.config/rofi/config.rasi.back /home/timo/.config/rofi/config.rasi

#--- starship.toml
sudo mv -fv /home/timo/.config/starship.toml.back /home/timo/.config/starship.toml

#--- mpris-proxy.service
sudo mv -fv /home/timo/.config/systemd/user/mpris-proxy.service.back /home/timo/.config/systemd/user/mpris-proxy.service

#--- config.json
sudo mv -fv /home/timo/.config/yay/config.json.back /home/timo/.config/yay/config.json

#--- youtube-dl
sudo rm -fv /home/timo/.config/youtube-dl
sudo mv -fv /home/timo/.config/youtube-dl.back /home/timo/.config/youtube-dl

#--- lightdm.conf
sudo mv -fv /etc/lightdm/lightdm.conf.back /etc/lightdm/lightdm.conf

#--- lightdm-webkit2-greeter.conf
sudo mv -fv /etc/lightdm/lightdm-webkit2-greeter.conf.back /etc/lightdm/lightdm-webkit2-greeter.conf

#--- pacman.conf
sudo mv -fv /etc/pacman.conf.back /etc/pacman.conf

#--- mirrorupgrade.hook
sudo mv -fv /etc/pacman.d/hooks/mirrorupgrade.hook.back /etc/pacman.d/hooks/mirrorupgrade.hook

#--- logind.conf
sudo mv -fv /etc/systemd/logind.conf.back /etc/systemd/logind.conf

#--- 00-keyboard.conf
sudo mv -fv /etc/X11/xorg.conf.d/00-keyboard.conf.back /etc/X11/xorg.conf.d/00-keyboard.conf

#--- 30-touchpad.conf
sudo mv -fv /etc/X11/xorg.conf.d/30-touchpad.conf.back /etc/X11/xorg.conf.d/30-touchpad.conf

#--- onedrive.service
sudo mv -fv /usr/lib/systemd/user/onedrive.service.back /usr/lib/systemd/user/onedrive.service

#--- timo
sudo mv -fv /var/spool/cron/timo.back /var/spool/cron/timo

#--- init.vim
sudo mv -fv /home/timo/.config/nvim/init.vim.back /home/timo/.config/nvim/init.vim

#--- snap-sync.service
sudo mv -fv /etc/systemd/system/snap-sync.service.back /etc/systemd/system/snap-sync.service

#--- snap-sync.timer
sudo mv -fv /etc/systemd/system/snap-sync.timer.back /etc/systemd/system/snap-sync.timer

