#!/bin/bash

#--- .aliases
if [ ! -L /home/timo/.aliases ]; then
	sudo mkdir -p /home/timo
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.aliases /home/timo/.aliases
fi

#--- .gitconfig
if [ ! -L /home/timo/.gitconfig ]; then
	sudo mkdir -p /home/timo
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.gitconfig /home/timo/.gitconfig
fi

#--- .xbindkeysrc
if [ ! -L /home/timo/.xbindkeysrc ]; then
	sudo mkdir -p /home/timo
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.xbindkeysrc /home/timo/.xbindkeysrc
fi

#--- .zshenv
if [ ! -L /home/timo/.zshenv ]; then
	sudo mkdir -p /home/timo
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.zshenv /home/timo/.zshenv
fi

#--- .zshrc
if [ ! -L /home/timo/.zshrc ]; then
	sudo mkdir -p /home/timo
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.zshrc /home/timo/.zshrc
fi

#--- index.theme
if [ ! -L /home/timo/.icons/default/index.theme ]; then
	sudo mkdir -p /home/timo/.icons/default
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.icons/default/index.theme /home/timo/.icons/default/index.theme
fi

#--- Scripts
if [ ! -L /home/timo/Scripts ]; then
	sudo mkdir -p /home/timo
	sudo mv -f /home/timo/Scripts /home/timo/Scripts.back
	sudo ln -sv /home/timo/Git/dotfiles/home/timo/Scripts /home/timo/Scripts
fi

#--- alacritty.yml
if [ ! -L /home/timo/.config/alacritty/alacritty.yml ]; then
	sudo mkdir -p /home/timo/.config/alacritty
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/alacritty/alacritty.yml /home/timo/.config/alacritty/alacritty.yml
fi

#--- aria2.conf
if [ ! -L /home/timo/.config/aria2/aria2.conf ]; then
	sudo mkdir -p /home/timo/.config/aria2
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/aria2/aria2.conf /home/timo/.config/aria2/aria2.conf
fi

#--- snippets
if [ ! -L /home/timo/.config/Code/User/snippets ]; then
	sudo mkdir -p /home/timo/.config/Code/User
	sudo mv -f /home/timo/.config/Code/User/snippets /home/timo/.config/Code/User/snippets.back
	sudo ln -sv /home/timo/Git/dotfiles/home/timo/.config/Code/User/snippets /home/timo/.config/Code/User/snippets
fi

#--- settings.ini
if [ ! -L /home/timo/.config/gtk-3.0/settings.ini ]; then
	sudo mkdir -p /home/timo/.config/gtk-3.0
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/gtk-3.0/settings.ini /home/timo/.config/gtk-3.0/settings.ini
fi

#--- config
if [ ! -L /home/timo/.config/i3/config ]; then
	sudo mkdir -p /home/timo/.config/i3
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/i3/config /home/timo/.config/i3/config
fi

#--- config
if [ ! -L /home/timo/.config/onedrive/config ]; then
	sudo mkdir -p /home/timo/.config/onedrive
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/onedrive/config /home/timo/.config/onedrive/config
fi

#--- business_shared_folders
if [ ! -L /home/timo/.config/onedrive-uni/business_shared_folders ]; then
	sudo mkdir -p /home/timo/.config/onedrive-uni
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/business_shared_folders /home/timo/.config/onedrive-uni/business_shared_folders
fi

#--- config
if [ ! -L /home/timo/.config/onedrive-uni/config ]; then
	sudo mkdir -p /home/timo/.config/onedrive-uni
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/config /home/timo/.config/onedrive-uni/config
fi

#--- picom.conf
if [ ! -L /home/timo/.config/picom/picom.conf ]; then
	sudo mkdir -p /home/timo/.config/picom
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/picom/picom.conf /home/timo/.config/picom/picom.conf
fi

#--- polybar
if [ ! -L /home/timo/.config/polybar ]; then
	sudo mkdir -p /home/timo/.config
	sudo mv -f /home/timo/.config/polybar /home/timo/.config/polybar.back
	sudo ln -sv /home/timo/Git/dotfiles/home/timo/.config/polybar /home/timo/.config/polybar
fi

#--- redshift.conf
if [ ! -L /home/timo/.config/redshift/redshift.conf ]; then
	sudo mkdir -p /home/timo/.config/redshift
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/redshift/redshift.conf /home/timo/.config/redshift/redshift.conf
fi

#--- config.rasi
if [ ! -L /home/timo/.config/rofi/config.rasi ]; then
	sudo mkdir -p /home/timo/.config/rofi
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/rofi/config.rasi /home/timo/.config/rofi/config.rasi
fi

#--- starship.toml
if [ ! -L /home/timo/.config/starship.toml ]; then
	sudo mkdir -p /home/timo/.config
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/starship.toml /home/timo/.config/starship.toml
fi

#--- mpris-proxy.service
if [ ! -L /home/timo/.config/systemd/user/mpris-proxy.service ]; then
	sudo mkdir -p /home/timo/.config/systemd/user
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/systemd/user/mpris-proxy.service /home/timo/.config/systemd/user/mpris-proxy.service
fi

#--- config.json
if [ ! -L /home/timo/.config/yay/config.json ]; then
	sudo mkdir -p /home/timo/.config/yay
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/yay/config.json /home/timo/.config/yay/config.json
fi

#--- youtube-dl
if [ ! -L /home/timo/.config/youtube-dl ]; then
	sudo mkdir -p /home/timo/.config
	sudo mv -f /home/timo/.config/youtube-dl /home/timo/.config/youtube-dl.back
	sudo ln -sv /home/timo/Git/dotfiles/home/timo/.config/youtube-dl /home/timo/.config/youtube-dl
fi

#--- lightdm.conf
if [ ! -L /etc/lightdm/lightdm.conf ]; then
	sudo mkdir -p /etc/lightdm
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf
fi

#--- lightdm-webkit2-greeter.conf
if [ ! -L /etc/lightdm/lightdm-webkit2-greeter.conf ]; then
	sudo mkdir -p /etc/lightdm
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/lightdm/lightdm-webkit2-greeter.conf /etc/lightdm/lightdm-webkit2-greeter.conf
fi

#--- pacman.conf
if [ ! -L /etc/pacman.conf ]; then
	sudo mkdir -p /etc
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/pacman.conf /etc/pacman.conf
fi

#--- mirrorupgrade.hook
if [ ! -L /etc/pacman.d/hooks/mirrorupgrade.hook ]; then
	sudo mkdir -p /etc/pacman.d/hooks
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/pacman.d/hooks/mirrorupgrade.hook /etc/pacman.d/hooks/mirrorupgrade.hook
fi

#--- logind.conf
if [ ! -L /etc/systemd/logind.conf ]; then
	sudo mkdir -p /etc/systemd
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/systemd/logind.conf /etc/systemd/logind.conf
fi

#--- 00-keyboard.conf
if [ ! -L /etc/X11/xorg.conf.d/00-keyboard.conf ]; then
	sudo mkdir -p /etc/X11/xorg.conf.d
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
fi

#--- 30-touchpad.conf
if [ ! -L /etc/X11/xorg.conf.d/30-touchpad.conf ]; then
	sudo mkdir -p /etc/X11/xorg.conf.d
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf
fi

#--- onedrive.service
if [ ! -L /usr/lib/systemd/user/onedrive.service ]; then
	sudo mkdir -p /usr/lib/systemd/user
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/usr/lib/systemd/user/onedrive.service /usr/lib/systemd/user/onedrive.service
fi

#--- timo
if [ ! -L /var/spool/cron/timo ]; then
	sudo mkdir -p /var/spool/cron
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/var/spool/cron/timo /var/spool/cron/timo
fi

#--- init.vim
if [ ! -L /home/timo/.config/nvim/init.vim ]; then
	sudo mkdir -p /home/timo/.config/nvim
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/home/timo/.config/nvim/init.vim /home/timo/.config/nvim/init.vim
fi

#--- snap-sync.service
if [ ! -L /etc/systemd/system/snap-sync.service ]; then
	sudo mkdir -p /etc/systemd/system
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.service /etc/systemd/system/snap-sync.service
fi

#--- snap-sync.timer
if [ ! -L /etc/systemd/system/snap-sync.timer ]; then
	sudo mkdir -p /etc/systemd/system
	sudo ln -sbv --suffix .back /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.timer /etc/systemd/system/snap-sync.timer
fi

