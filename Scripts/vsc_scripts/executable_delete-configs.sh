#!/bin/bash

#--- .aliases
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.aliases

#--- .gitconfig
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.gitconfig

#--- .xbindkeysrc
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.xbindkeysrc

#--- .zshenv
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.zshenv

#--- .zshrc
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.zshrc

#--- index.theme
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.icons/default/index.theme

#--- Scripts
sudo rm -rfv /home/timo/Git/dotfiles/home/timo/Scripts

#--- alacritty.yml
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/alacritty/alacritty.yml

#--- aria2.conf
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/aria2/aria2.conf

#--- snippets
sudo rm -rfv /home/timo/Git/dotfiles/home/timo/.config/Code/User/snippets

#--- settings.ini
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/gtk-3.0/settings.ini

#--- config
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/i3/config

#--- config
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive/config

#--- business_shared_folders
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/business_shared_folders

#--- config
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/onedrive-uni/config

#--- picom.conf
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/picom/picom.conf

#--- polybar
sudo rm -rfv /home/timo/Git/dotfiles/home/timo/.config/polybar

#--- redshift.conf
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/redshift/redshift.conf

#--- config.rasi
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/rofi/config.rasi

#--- starship.toml
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/starship.toml

#--- mpris-proxy.service
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/systemd/user/mpris-proxy.service

#--- config.json
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/yay/config.json

#--- youtube-dl
sudo rm -rfv /home/timo/Git/dotfiles/home/timo/.config/youtube-dl

#--- lightdm.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/lightdm/lightdm.conf

#--- lightdm-webkit2-greeter.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/lightdm/lightdm-webkit2-greeter.conf

#--- pacman.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/pacman.conf

#--- mirrorupgrade.hook
sudo rm -fv /home/timo/Git/dotfiles/etc/pacman.d/hooks/mirrorupgrade.hook

#--- logind.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/systemd/logind.conf

#--- 00-keyboard.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/00-keyboard.conf

#--- 30-touchpad.conf
sudo rm -fv /home/timo/Git/dotfiles/etc/X11/xorg.conf.d/30-touchpad.conf

#--- onedrive.service
sudo rm -fv /home/timo/Git/dotfiles/usr/lib/systemd/user/onedrive.service

#--- timo
sudo rm -fv /home/timo/Git/dotfiles/var/spool/cron/timo

#--- init.vim
sudo rm -fv /home/timo/Git/dotfiles/home/timo/.config/nvim/init.vim

#--- snap-sync.service
sudo rm -fv /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.service

#--- snap-sync.timer
sudo rm -fv /home/timo/Git/dotfiles/etc/systemd/system/snap-sync.timer

