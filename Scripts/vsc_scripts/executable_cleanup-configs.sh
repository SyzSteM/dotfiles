#!/bin/bash

#--- .aliases
sudo rm -fv /home/timo/.aliases.back

#--- .gitconfig
sudo rm -fv /home/timo/.gitconfig.back

#--- .xbindkeysrc
sudo rm -fv /home/timo/.xbindkeysrc.back

#--- .zshenv
sudo rm -fv /home/timo/.zshenv.back

#--- .zshrc
sudo rm -fv /home/timo/.zshrc.back

#--- index.theme
sudo rm -fv /home/timo/.icons/default/index.theme.back

#--- Scripts
sudo rm -rfv /home/timo/Scripts.back

#--- alacritty.yml
sudo rm -fv /home/timo/.config/alacritty/alacritty.yml.back

#--- aria2.conf
sudo rm -fv /home/timo/.config/aria2/aria2.conf.back

#--- snippets
sudo rm -rfv /home/timo/.config/Code/User/snippets.back

#--- settings.ini
sudo rm -fv /home/timo/.config/gtk-3.0/settings.ini.back

#--- config
sudo rm -fv /home/timo/.config/i3/config.back

#--- config
sudo rm -fv /home/timo/.config/onedrive/config.back

#--- business_shared_folders
sudo rm -fv /home/timo/.config/onedrive-uni/business_shared_folders.back

#--- config
sudo rm -fv /home/timo/.config/onedrive-uni/config.back

#--- picom.conf
sudo rm -fv /home/timo/.config/picom/picom.conf.back

#--- polybar
sudo rm -rfv /home/timo/.config/polybar.back

#--- redshift.conf
sudo rm -fv /home/timo/.config/redshift/redshift.conf.back

#--- config.rasi
sudo rm -fv /home/timo/.config/rofi/config.rasi.back

#--- starship.toml
sudo rm -fv /home/timo/.config/starship.toml.back

#--- mpris-proxy.service
sudo rm -fv /home/timo/.config/systemd/user/mpris-proxy.service.back

#--- config.json
sudo rm -fv /home/timo/.config/yay/config.json.back

#--- youtube-dl
sudo rm -rfv /home/timo/.config/youtube-dl.back

#--- lightdm.conf
sudo rm -fv /etc/lightdm/lightdm.conf.back

#--- lightdm-webkit2-greeter.conf
sudo rm -fv /etc/lightdm/lightdm-webkit2-greeter.conf.back

#--- pacman.conf
sudo rm -fv /etc/pacman.conf.back

#--- mirrorupgrade.hook
sudo rm -fv /etc/pacman.d/hooks/mirrorupgrade.hook.back

#--- logind.conf
sudo rm -fv /etc/systemd/logind.conf.back

#--- 00-keyboard.conf
sudo rm -fv /etc/X11/xorg.conf.d/00-keyboard.conf.back

#--- 30-touchpad.conf
sudo rm -fv /etc/X11/xorg.conf.d/30-touchpad.conf.back

#--- onedrive.service
sudo rm -fv /usr/lib/systemd/user/onedrive.service.back

#--- timo
sudo rm -fv /var/spool/cron/timo.back

#--- init.vim
sudo rm -fv /home/timo/.config/nvim/init.vim.back

#--- snap-sync.service
sudo rm -fv /etc/systemd/system/snap-sync.service.back

#--- snap-sync.timer
sudo rm -fv /etc/systemd/system/snap-sync.timer.back

