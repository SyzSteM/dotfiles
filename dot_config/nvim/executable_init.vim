let mapleader=","

set number
set relativenumber
highlight LineNr ctermfg=DarkGrey

call plug#begin(stdpath('data'))

" sensible vim config
Plug 'tpope/vim-sensible'

" file browser in vim
Plug 'preservim/NERDTree'

" syntax highlighting for many files
Plug 'sheerun/vim-polyglot'

" generate matching pairs of brackets
Plug 'jiangmiao/auto-pairs'

" bottom bar theme
Plug 'itchyny/lightline.vim'

" rainbow parentheses
Plug 'luochen1990/rainbow'

" autoformatting
Plug 'Chiel92/vim-autoformat'
" autoformat document on save
"au BufWrite * :Autoformat

let g:rainbow_active = 1

call plug#end()
