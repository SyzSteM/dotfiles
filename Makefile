LOGFILE=/tmp/dotfiles.log

default: all
help:
	@echo 'Management commands for dotfiles'
	@echo
	@echo 'Usage:'
	@echo '    make all                Run all.'
	@echo
	@echo '    Logs are stored in      $(LOGFILE)'
	@echo
	@echo '    Author                  Timo Tabertshofer (gitlab.com/SyzSteM)'

install-chezmoi:
	@echo "Installing chezmoi..."
	bash ./chezmoi_scripts/install_chezmoi.sh | tee -a $(LOGFILE) || exit 1

install-bitwarden:
	@echo "Installing bitwarden cli..."
	bash ./chezmoi_scripts/install_bitwarden.sh | tee -a $(LOGFILE) || exit 1

install-rustup:
	@echo "Installing rust and cargo via rustup..."
	bash ./chezmoi_scripts/install_rustup.sh | tee -a $(LOGFILE) || exit 1

install-neovim:
	@echo "Installing neovim, vim-plug and neovim plugins..."
	bash ./chezmoi_scripts/install_neovim.sh | tee -a $(LOGFILE) || exit 1

add-repos-zypper:
	@echo "Add zypper repos..."
	bash ./chezmoi_scripts/add_repos_zypper.sh | tee -a $(LOGFILE) || exit 1

install-packages-zypper:
	@echo "Installing zypper packages..."
	bash ./chezmoi_scripts/install_packages_zypper.sh | tee -a $(LOGFILE)

install-packages-cargo:
	@echo "Installing cargo packages..."
	bash ./chezmoi_scripts/install_packages_cargo.sh | tee -a $(LOGFILE) || exit 1

install-packages-flatpak:
	@echo "Installing flatpak packages..."
	bash ./chezmoi_scripts/install_packages_flatpak.sh | tee -a $(LOGFILE) || exit 1

install-konsave:
	@echo "Installing konsave..."
	pip install --user --break-system-packages konsave

install-zsh-plugins:
	@echo "Installing oh-my-zsh and zsh plugins..."
	bash ./chezmoi_scripts/install_ohmyzsh.sh | tee -a $(LOGFILE) || exit 1
	bash ./chezmoi_scripts/install_zsh_plugins.sh | tee -a $(LOGFILE) || exit 1

create-directories:
	@echo "Creating user directory structures..."
	bash ./chezmoi_scripts/create_directories.sh | tee -a $(LOGFILE) || exit 1

enable-services:
	@echo "Enabling services..."
	bash ./chezmoi_scripts/enable_services.sh | tee -a $(LOGFILE) || exit 1

chezmoi-init:
	@echo "Initializing chezmoi..."
	chezmoi init -S ${CURDIR} -v

chezmoi-apply:
	@echo "Applying chezmoi..."
	chezmoi apply

konsave-update:
	@echo "Updating dotfiles repo from local sources..."
	$(HOME)/.local/bin/konsave -r opensuse && $(HOME)/.local/bin/konsave -s opensuse
	chezmoi add $(HOME)/.config/konsave

konsave-apply:
	@echo "Applying konsave..."
	$(HOME)/.local/bin/konsave -a opensuse

setup-user-groups:
	@echo "Adding user to groups and editing sudoers file..."
	bash ./chezmoi_scripts/setup_user_groups.sh

setup-nvidia:
	@echo "Setting up nvidia drivers..."
	bash ./chezmoi_scripts/setup_nvidia.sh

fix-x11-nvidia:
	@echo "Fixing x11 nvidia drivers..."
	bash ./chezmoi_scripts/fix_x11_nvidia.sh

setup-tuxedo:
	@echo "Setting up tuxedo drivers..."
	bash ./chezmoi_scripts/setup_tuxedo.sh

copy-files-from-nextcloud:
	@echo "Copying files after install..."
	bash ./chezmoi_scripts/copy_files_from_nextcloud.sh

copy-files-to-nextcloud:
	@echo "Copying files to nextcloud..."
	bash ./chezmoi_scripts/copy_files_to_nextcloud.sh

ensure-deps:
	@echo "Ensuring dependencies..."
	$(MAKE) setup-user-groups
	$(MAKE) install-chezmoi
	$(MAKE) install-bitwarden
	$(MAKE) install-rustup
	$(MAKE) create-directories

install-packages:
	@echo "Installing packages from zypper, cargo and flatpak..."
	$(MAKE) add-repos-zypper
	$(MAKE) install-packages-zypper
	$(MAKE) install-packages-cargo
	$(MAKE) install-packages-flatpak

post-packages:
	@echo "Installing plugins for installed packages..."
	$(MAKE) install-konsave
	$(MAKE) install-neovim
	$(MAKE) install-zsh-plugins
	$(MAKE) enable-services

all:
	$(MAKE) ensure-deps
	$(MAKE) install-packages
	$(MAKE) post-packages
	$(MAKE) chezmoi-init
	$(MAKE) chezmoi-apply

packages:
	$(MAKE) ensure-deps
	$(MAKE) install-packages
	$(MAKE) post-packages

nvidia:
	$(MAKE) setup-nvidia
	$(MAKE) fix-x11-nvidia

tuxedo:
	$(MAKE) setup-tuxedo

from-nextcloud:
	$(MAKE) copy-files-from-nextcloud

to-nextcloud:
	$(MAKE) copy-files-to-nextcloud

testing:
	bash chezmoi_scripts/testing.sh | tee -a $(LOGFILE) || exit 1
