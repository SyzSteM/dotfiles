# Dotfiles managed by chezmoi

Install `git` and `make` on your system to clone and execute the repository.

```shell
sudo zypper install --no-confirm git make
```

Create the needed folder and clone the repository with

```shell
mkdir -p ~/Git
cd ~/Git
git clone https://gitlab.com/SyzSteM/dotfiles.git
cd dotfiles
```

and execute `make` inside the folder. This defaults to `make all` which kicks off the setup process.

After successfully executing all steps you can choose to run `chezmoi` with secret handling enabled via Bitwarden.
Log into the Bitwarden CLI with `bw login`, unlock Bitwarden with `bw unlock` and set `secrets` to `true`
in `~/.config/chezmoi/chezmoi.yaml`. After that run

```shell
chezmoi apply -v
```

again to add all required secrets to your system.

Now the right SSH- and GPG-keys are installed, and you can change the line in `~/Git/dotfiles/.git/config`:

```shell
url = git@gitlab.com:SyzSteM/dotfiles.git
```

The dotfiles repository will now be managed via SSH and not HTTPS.

# Manual steps

- log into Bitwarden, log into Firefox
- setup OpenVPN/WireGuard

Software links:

- [Jetbrains Toolbox](https://www.jetbrains.com/toolbox-app/)
- [ICAClient](https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html) 
- [Apache Tomcat](https://tomcat.apache.org/download-90.cgi)
