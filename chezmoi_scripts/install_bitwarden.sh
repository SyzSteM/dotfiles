#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

if ! [ "$(command -v bw)" ]; then
  info "Installing bitwarden cli"

  if ! [ "$(command -v npm)" ]; then
    info "Installing npm"
    sudo zypper install --no-confirm nodejs-devel
  else
    warn "Nodejs is already installed, skipping"
  fi

  sudo npm install -g @bitwarden/cli

else
  warn "Bitwarden cli already installed, skipping"
fi
