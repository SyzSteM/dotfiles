#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Installing chezmoi"
if ! [ "$(command -v chezmoi)" ]; then
  cd "$HOME"
  sh -c "$(curl -fsLS git.io/chezmoi)"
else
  warn "Chezmoi is already installed, skipping"
fi
