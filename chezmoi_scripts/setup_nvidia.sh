#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

info "Adding NVIDIA repository"
sudo zypper install openSUSE-repos-Tumbleweed-NVIDIA
sudo zypper --gpg-auto-import-keys refresh-services

info "Installing NVIDIA drivers"
sudo zypper install --auto-agree-with-licenses -y nvidia-drivers-G06

info "Installing nvtop and NVIDIA utils"
sudo zypper install --auto-agree-with-licenses nvtop nvidia-utils-G06 nvidia-container-toolkit

info "Adding CUDA and cuDNN repos"
sudo zypper addrepo https://developer.download.nvidia.com/compute/cuda/repos/opensuse15/x86_64/cuda-opensuse15.repo || true
sudo zypper --gpg-auto-import-keys refresh

info "Installing CUDA and cuDNN"
sudo zypper install --auto-agree-with-licenses cuda-12-8 cudnn9-cuda-12-8
