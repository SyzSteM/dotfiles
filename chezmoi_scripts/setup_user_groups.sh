#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

if [[ ! $(groups) =~ "wheel" ]]; then
  info "Adding user to group wheel"
  sudo usermod -aG wheel "$USER"

  info "Editing sudoers file"
  sudo visudo

  info "Editing hostname"
  sudo vim /etc/hostname

  warn "Sudoers file edited, hostname edited, user added to group wheel"
  error "Rebooting in 5 seconds to apply changes"
  sleep 5
  sudo reboot
  exit 1
else
  warn "User already in group wheel, skipping"
fi

if [ "$(command -v docker)" ]; then
  if [[ ! $(groups) =~ "docker" ]]; then
    info "Adding user to group docker"
    sudo usermod -aG docker "$USER"
  else
    warn "User already in group docker, skipping"
  fi
else
  warn "Docker not installed, skipping"
fi
