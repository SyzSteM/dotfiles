#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Installing neovim"
if ! [ "$(command -v nvim)" ]; then
  sudo zypper install  neovim
else
  warn "Neovim is already installed, skipping"
fi

info "Installing vim-plug for neovim"
if ! [ -f "$HOME/.local/share/nvim/site/autoload/plug.vim" ]; then
  sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
else
  warn "Vim-plug for neovim already installed, skipping"
fi

info "Installing neovim plugins"
nvim +PlugInstall +qall --headless
