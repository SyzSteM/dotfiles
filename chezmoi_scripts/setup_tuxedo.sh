#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

info "Adding Tuxedo repository"
sudo zypper addrepo https://rpm.tuxedocomputers.com/opensuse/15.5/repo-tuxedo-computers.repo || true
sudo zypper --gpg-auto-import-keys refresh

info "Installing Tuxedo packages"
sudo zypper install tuxedo-drivers tuxedo-tomte tuxedo-control-center
