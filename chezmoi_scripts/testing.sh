#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Info level log messing testing"
warn "Warn level log messing testing"
error "Error level log messing testing"

error "$DIR"

if [[ $(groups) =~ "wheel" ]]; then
  info "yes"
else
  warn "no"
fi
