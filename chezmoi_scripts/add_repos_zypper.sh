#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

sudo zypper install --no-confirm openSUSE-repos-Tumbleweed
sudo zypper --gpg-auto-import-keys refresh-services

sudo zypper ar -cfp 90 "https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/Essentials/" packman-essentials
sudo zypper --gpg-auto-import-keys refresh

sudo zypper dup --no-confirm --from packman-essentials --allow-vendor-change --recommends
