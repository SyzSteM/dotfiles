#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

if command -v docker &>/dev/null; then
  info "Docker is installed, enabling and starting the service"
  sudo systemctl enable --now docker
else
  warn "Docker is not installed."
fi
