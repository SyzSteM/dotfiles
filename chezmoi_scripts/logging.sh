#!/usr/bin/env bash
# shellcheck disable=2034
# shellcheck disable=SC1091
set -euo pipefail

#--- STYLING VARIABLES
BOLD=$(tput bold)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
PURPLE=$(tput setaf 5)
BLUE=$(tput setaf 6)
WHITE=$(tput setaf 7)
RESET=$(tput sgr0)

function info() {
  echo -e "$BOLD::$RESET $BLUE$1...$RESET"
}

function warn() {
  echo -e "$BOLD:: $YELLOW$1$RESET"
}

function error() {
  echo -e "$BOLD:: $RED$1$RESET"
}

function yellow_bold() {
  echo -e "$BOLD$YELLOW$1$RESET"
}
