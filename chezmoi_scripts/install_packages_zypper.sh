#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Removing unwanted zypper packages"
xargs --arg-file="$DIR/chezmoi_resources/packages_zypper_remove" sudo zypper remove --no-confirm --clean-deps || true

info "Locking unwanted zypper packages"
xargs --arg-file="$DIR/chezmoi_resources/packages_zypper_remove" sudo zypper addlock

info "Removing unwanted zypper patterns"
xargs --arg-file="$DIR/chezmoi_resources/patterns_zypper_remove" sudo zypper remove --no-confirm --type pattern --clean-deps || true

info "Locking unwanted zypper patterns"
xargs --arg-file="$DIR/chezmoi_resources/patterns_zypper_remove" sudo zypper addlock --type pattern

info "Installing zypper packages"
sudo zypper install --no-confirm $(cat $DIR/chezmoi_resources/packages_zypper)

info "Installing zypper patterns"
xargs --arg-file="$DIR/chezmoi_resources/patterns_zypper" sudo zypper install --no-confirm --type pattern
