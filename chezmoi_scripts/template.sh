#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"
