#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

info "Applying /etc/X11/xorg.conf"
if [ -f /etc/X11/xorg.conf ]; then
  info "Backing up /etc/X11/xorg.conf"
  sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.bak
fi
sudo cp -f "$HOME/Nextcloud/Documents/opensuse_installation/xorg.conf" /etc/X11/xorg.conf

info "Applying /etc/environment"
if [ -f /etc/environment ]; then
  info "Backing up /etc/environment"
  sudo mv /etc/environment /etc/environment.bak
fi
sudo cp -f "$HOME/Nextcloud/Documents/opensuse_installation/environment" /etc/environment
