#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Installing cargo packages"
xargs --arg-file=chezmoi_resources/packages_cargo "$HOME/.cargo/bin/cargo" install
