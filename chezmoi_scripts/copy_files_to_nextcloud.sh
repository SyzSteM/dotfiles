#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

info "Copying Thunderbird profiles"
cp -rf "$HOME/.thunderbird" "$HOME/Nextcloud/Documents/opensuse_installation/.thunderbird"

info "Copying .m2 config"
cp -f "$HOME/.m2/settings.xml" "$HOME/Nextcloud/Documents/opensuse_installation/settings.xml"

info "Copying /etc/hosts"
sudo cp -f /etc/hosts "$HOME/Nextcloud/Documents/opensuse_installation/hosts"

mkdir -p "$HOME/Nextcloud/Documents/opensuse_installation/.local/share"

info "Copying ~/.local/share/fonts"
cp -rf "$HOME/.local/share/fonts" "$HOME/Nextcloud/Documents/opensuse_installation/.local/share/fonts"

if [ -f /etc/X11/xorg.conf ]; then
  info "Copying /etc/X11/xorg.conf"
  sudo cp -f /etc/X11/xorg.conf "$HOME/Nextcloud/Documents/opensuse_installation/xorg.conf"
fi

if [ -f /etc/environment ]; then
  info "Copying /etc/environment"
  sudo cp -f /etc/environment "$HOME/Nextcloud/Documents/opensuse_installation/environment"
fi
