#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Creating user folder structures for Git"
mkdir --parents "$HOME/Git/dotfiles"
mkdir --parents "$HOME/Git/temp"

info "Creating user folder structures for Development"
mkdir --parents "$HOME/Development/personal"
mkdir --parents "$HOME/Development/plincs"
mkdir --parents "$HOME/Development/university"

info "Creating user folder structures for Applications"
mkdir --parents "$HOME/Applications"

info "Creating user folder structures for Nextcloud"
mkdir --parents "$HOME/Nextcloud"
