#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Installing rustup"
if ! [ "$(command -v rustup)" ]; then
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
else
  warn "Rustup is already installed, skipping"
fi

info "Installing nightly rust"
"$HOME/.cargo/bin/rustup" toolchain add nightly
