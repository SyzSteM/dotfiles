#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)")
# shellcheck source=./logging.sh
source "$DIR/chezmoi_scripts/logging.sh"

info "Applying Thunderbird profiles"
cp -rf "$HOME/Nextcloud/Documents/opensuse_installation/.thunderbird" "$HOME/.thunderbird"

info "Applying .m2 config"
mkdir -p "$HOME/.m2"
cp -f "$HOME/Nextcloud/Documents/opensuse_installation/settings.xml" "$HOME/.m2/settings.xml"

info "Applying /etc/hosts"
if [ -f /etc/hosts ]; then
  info "Backing up /etc/hosts"
  sudo mv /etc/hosts /etc/hosts.bak
fi
sudo cp -f "$HOME/Nextcloud/Documents/opensuse_installation/hosts" /etc/hosts

info "Applying ~/.local/share/fonts"
if [ -d "$HOME/.local/share/fonts" ]; then
  info "Backing up ~/.local/share/fonts"
  mv "$HOME/.local/share/fonts" "$HOME/.local/share/fonts.bak"
fi
cp -rf "$HOME/Nextcloud/Documents/opensuse_installation/.local/share/fonts" "$HOME/.local/share/fonts"
