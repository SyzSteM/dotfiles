#!/usr/bin/env bash
set -euo pipefail

DIR=$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)")
source "$DIR/chezmoi_scripts/logging.sh"

info "Installing flatpak system package"
sudo zypper install --no-confirm flatpak

info "Adding FlatHub remote source to flatpak"
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update

info "Installing flatpak packages"
flatpak install --user --assumeyes --noninteractive --or-update $(cat $DIR/chezmoi_resources/packages_flatpak)
